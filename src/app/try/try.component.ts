import { TryService } from './../try.service';
import { Component, Input, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-try',
  templateUrl: './try.component.html',
  styleUrls: ['./try.component.css']
})
export class TryComponent implements OnInit {
  @Input() data:any;

  name;
  price;
  $key;
  showdelete:boolean = false;
  areYouSure:boolean = false;
  isInInventory:boolean = false;


  delete(){
    console.log(this.$key)
    this.TryService.delete(this.$key)
  }

  show(){
    if(!this.areYouSure)
    this.showdelete = true;
  }

  hide(){
    this.showdelete = false;
  }  

  showAreYouSure(){
     this.areYouSure = true;
     this.showdelete = false;
  }

  hideAreYouSure(){
    this.areYouSure = false;
  }  

  changeInventory(){
    console.log(this.isInInventory);
    this.TryService.updateInevntory(this.$key, this.isInInventory);
   
  }



  constructor(private TryService:TryService) { }

  ngOnInit(): void {
    this.$key = this.data.$key;     
    this.name = this.data.name;
    this.price = this.data.price;
    this.isInInventory = this.data.inventory;  
  }

}
