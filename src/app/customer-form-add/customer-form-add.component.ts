import { ActivatedRoute, Router } from '@angular/router';
import { CustomersService } from './../customers.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { CustomerPredictionService } from './../customer-prediction.service';
import { Customer } from '../interfaces/customer';



@Component({
  selector: 'app-customer-form-add',
  templateUrl: './customer-form-add.component.html',
  styleUrls: ['./customer-form-add.component.css']
})
export class CustomerFormAddComponent implements OnInit {

  UserId;
  name;
  income;
  education;
  willReturn;


  Predict(){
    this.CustomerPredictionService.classify(this.education,this.income).subscribe(
      res => {
        console.log(res)
      if (res>0.5){
        var result = 'yes'
      }
      else{
        var result = 'no'
      }
      this.willReturn = result
  
      })
    this.add()
  }

  add(){
    this.CustomersService.addCustomerWithPred(this.UserId, this.name, this.income, this.education,this.willReturn)
 // this.navigate()
  }

  navigate(){
    this.router.navigate(['/showpred',this.name, this.education, this.education, this.willReturn])
  }
  
  constructor(private CustomersService:CustomersService, private router:Router, private AuthService:AuthService, private CustomerPredictionService:CustomerPredictionService) { }

  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
      this.UserId = user.uid;
    })
  }

}
