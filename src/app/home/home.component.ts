import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userEmail;


ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
      this.userEmail = user.email;
    })
  }

  constructor(private AuthService:AuthService) { }


}
