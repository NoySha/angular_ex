import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';
import { MatCardTitleGroup } from '@angular/material/card';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"},
  {title:'War and Peace', author:'Leo Tolstoy',summary:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."},
   {title:'The Magic Mountain', author:'Thomas Mann', summary:"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."}]

  public addBooks(){
    setInterval(()=>this.books.push({title:'A new one',author:'New author',summary:'Short summary'}),2000);
  }

/* old function:
   public getBooks(){
     const booksObservable = new Observable(observer => {
        setInterval(()=>observer.next(this.books),500)
     });
     return booksObservable; //מחזירים משתנה מסוג observable 
    }
*/
    bookCollection:AngularFirestoreCollection;
    userCollection:AngularFirestoreCollection = this.db.collection('users')
    
 /*   
    public getBooks(userId){ // אנו רוצים ספרים של יוזר מסוים, ולכן נכניס את היוזר כמשתנה 
      this.bookCollection = this.db.collection(`users/${userId}/books`) //רפרנס לקולקשיין של בוקס (המיקום שלו)
      return this.bookCollection.snapshotChanges().pipe(map( 
        // snapshotChanges - האובסרבל שמאזין לשינויים בקולקשיין של בוקס וכשיש שינוי הוא מונה מחדש את רשימת הספרים
        // pipe היא פונקציה שבעזרתה ניתן לבצע פונקציות על האובסרבל - בדרך כלל יהיו שתי פונקציות אחת להצלחה ואחת לכשלון. פה נתייחס רק להצלחה
        // map - העברת מיפוי 
        collection => collection.map(
          document => {
            const data = document.payload.doc.data(); //  כל השדות של כל ספר נמצאים בדטא- payload.doc.data - פוקנציה שמורה של פייר סטור
            data.id = document.payload.doc.id; // מוסיפים לדטא את השדה איי די של הספר
            console.log(data)
            return data; // בדטא יש שלוש שדות: title, author, id
          }
        )
      )) 
     }
*/

public getBooksBackPage(userId){ 
  this.bookCollection = this.db.collection(`users/${userId}/books`, 
  ref => ref.orderBy('title', 'asc').limit(4)); 
  return this.bookCollection.snapshotChanges()
 }

// פיצול פונקציה גאט בוקס לסרוויס ולקומפוננט 04.01 בשביל דפדוף
public getBooks(userId, startAfter){ 
  this.bookCollection = this.db.collection(`users/${userId}/books`, 
  ref => ref.orderBy('title', 'asc').limit(4).startAfter(startAfter)); 
  return this.bookCollection.snapshotChanges()
 }

     // אם לא כותבים כלום הפונקציה היא פבליק
     deleteBook(Userid:string, id:string){
       this.db.doc(`users/${Userid}/books/${id}`).delete();
     }

     updateBook(userId:string, id:string, title:string, author:string){
       this.db.doc(`users/${userId}/books/${id}`).update(
         {
           title:title, // שם של השדה:הערך של השדה שהוכנס לפונ
           author:author
         }
       )
     }

     addBook(userId:string, title:string, author:string){ //אפשר לעשות באותה דרך שעשינו בפונקציה הקודמת עם הנתיב
       const book = {title:title, author:author};
       this.userCollection.doc(userId).collection('books').add(book);
     }
    
   /*
   public getBooks(){
     return this.books;
   }
   */
  
 /*
   bookCollection:AngularFirestoreCollection;

   getBooks(userId):Observable<any[]>{

   }
*/
  constructor(private db:AngularFirestore) { } // יש לנו משתנה די בי במחלקה שהיא מסוג אנגולר פייר סטור
}


