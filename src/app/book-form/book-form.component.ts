import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Book } from '../interfaces/book';


@Component({
  selector: 'bookform', // השם של האלמנט כאשר נטמיע אותו באלמנט אחר
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @Input() title:string;
  @Input() author:string;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Book>();
  @Output() closeEdit = new EventEmitter<null>(); // לא מעבירים פה מידע אלא רק מעדכנים את אלמנט האב שהאירוע התרחש
  errorMessage:string;

  updateParent(){ // שולחת את הנתונים לאלמנט האב
    let book:Book = {id:this.id, title:this.title, author:this.author};
    this.update.emit(book); // אמיט משדר החוצה את בוק, בגלל זה הגדרנו את אפדייט כאיוונט אמיט
    // הבן יכול לעדכן את האב רק באמצעות איוונט
    // בפונקציה למטה, אם אנו בטופס הוספת ספר, אז השדות יהיו ריקים ולא ישמרו מהוספת הספר הקודם
    if(this.formType == "Add book"){
      this.title = null;
      this.author = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }

  checkValidTitle(title){
    console.log(typeof(title));
    if (title>0 && title<24){
      this.updateParent()
      this.errorMessage = ''
    }
    else{
      this.errorMessage = 'Title must be between 0-24';
    }

  } 

  constructor() { }

  ngOnInit(): void {
  }

}
