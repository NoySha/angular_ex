export interface Weather {
    name:string,
    country:string,
    image:string,
    description:string,
    temperature:number,
    lat?:number,
    lon?:number //? אופציונלי - אם המידע יתקבל ללא אחד מהם לא תהיה הודעת שגיאה



    
}
