import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  itemsCollection:AngularFirestoreCollection;


  public getItems(userId){ 
    this.itemsCollection = this.db.collection(`users/${userId}/items`) 
    return this.itemsCollection.snapshotChanges().pipe(map( 
       collection => collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id; 
          console.log(data)
          return data; 
        }
        )
      )) 
     }

     public updateStock(userId,id, inStock){
      this.db.doc(`users/${userId}/items/${id}`).update(
        {inStock:inStock}
      )
    }

    public updatePrice(userId,id, price){
      this.db.doc(`users/${userId}/items/${id}`).update(
        {price:price}
      )
    }


    public deleteItems(userId,id){
      this.db.doc(`users/${userId}/items/${id}`).delete()
    }
    

  constructor(private db:AngularFirestore) { }
}
