import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>; //יצרנו אינטרפייס של יוזר. האובסרבל יהיה מסוג יוזר או מסוג נאל
  
  signUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password)
  }
  //הפונקציה מקבלת מייל וסיסמא
  // שימוש בסרוויס של אנגולר AngularFireAuth :afAuth
  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email,password)
  }

  logout(){
    this.afAuth.signOut();
  }

  getUser():Observable<User | null>{
    return this.user;
    // הפונקציה מחזירה אובסרבל של יוזר שמחובר כעת, ואם אין היא מחזירה נאל
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState; // יצרנו חיבור בין היוזר לבין האובסרבל 
  }
}