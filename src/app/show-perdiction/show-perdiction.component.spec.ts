import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPerdictionComponent } from './show-perdiction.component';

describe('ShowPerdictionComponent', () => {
  let component: ShowPerdictionComponent;
  let fixture: ComponentFixture<ShowPerdictionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowPerdictionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPerdictionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
