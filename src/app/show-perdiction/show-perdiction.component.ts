import { CustomersService } from './../customers.service';
import { CustomerPredictionService } from './../customer-prediction.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';


@Component({
  selector: 'app-show-perdiction',
  templateUrl: './show-perdiction.component.html',
  styleUrls: ['./show-perdiction.component.css']
})
export class ShowPerdictionComponent implements OnInit {
  income;
  name;
  education;
  prob;
  result;
  userId;
  select;

  options = ['yes', 'no'];
  selectedValue =true;

  change(){
    console.log('in change')
  }


  constructor(private route:ActivatedRoute, private CustomerPredictionService:CustomerPredictionService, private CustomersService:CustomersService, private AuthService:AuthService) { }

  ngOnInit(): void {
 
    this.name = this.route.snapshot.params.name;
    this.education = this.route.snapshot.params.education;


  }

}

