import { ItemsPredictionService } from './../items-prediction.service';
import { ItemsService } from './../items.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';


@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  userId;
  items = [];
  changeText: boolean;
  clickDelete =[];
  clickclose =[];
  clickDelete2 =[];
  displayedColumns: string[] = ['name', 'price', 'inStock', 'delete','result'];
  showdelete:boolean = true;
  areYouSure:boolean = false;
  prices = [9,15,100,200,300];
  selectedValue = null;

  updateStock(index){
    console.log( this.items[index].inStock)
    this.ItemsService.updateStock(this.userId,this.items[index].id, this.items[index].inStock)
  }

  updatePrice(index){
    this.ItemsService.updatePrice(this.userId,this.items[index].id, this.items[index].price)
  }

  predict(index){
    this.ItemsPredictionService.classify(this.items[index].price,this.items[index].inStock).subscribe(
      res=> {
        console.log(res);
        this.items[index].predict = res;
      }
    )
  }


  delete(index){
    this.ItemsService.deleteItems(this.userId, this.items[index].id)
  }

  showAreYouSure(){
    this.areYouSure = true;
    this.showdelete = false;
 }

 hideAreYouSure(){
   this.areYouSure = false;
 }  


  constructor(public AuthService:AuthService, private ItemsService:ItemsService,private ItemsPredictionService:ItemsPredictionService) {
    this.changeText = false;
   }

  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
      this.userId = user.uid
      this.ItemsService.getItems(this.userId).subscribe(
        items =>{
        this.items = items
        console.log(this.userId)
        console.log(this.items)

      })
      })
  }

}
