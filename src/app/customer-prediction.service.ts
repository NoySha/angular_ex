import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CustomerPredictionService {

  
 // url API get way (connect to lamda function named example_test)
 // https://jz45oi2bq8.execute-api.us-east-1.amazonaws.com/example_test

 private url = "https://jz45oi2bq8.execute-api.us-east-1.amazonaws.com/example_test"

 //נשלח נתונים לאיי פי אי ונקבל חזרה תוצאה
 classify(education:number, income:number):Observable<any>{
   //המבנה צריך להיות בדיוק זה שהאיי פי אי רוצה לקבל - לקחת את הג'סון שהוא מחזיר ולשים בביוטיפייר
     let json = {
       'data': {
         'years': education,
         'income': income
       }
     }
     //נהפוך לסטרינג בשביל להעביר אותו ברשת
     let body = JSON.stringify(json);
  return this.HttpClient.post<any>(this.url, body).pipe(
       map(res => {
         console.log(res); //מסתכלים על הראס ורואים איך שולפים ממנה את הנתונים שאנו צריכים
         let final:string = res.body; // we want only the body
         return final; 
       })
     )
 
   }
 
  constructor(private HttpClient:HttpClient) { }
}
