import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private url = "https://a90f52kzoa.execute-api.us-east-1.amazonaws.com/beta" //url of API getway
  categories:object = {0:'Business', 1:'Entertainment', 2:'Politics', 3:'Sport', 4:'Tech'};

  //נשלח נתונים לאיי פי אי ונקבל חזרה תוצאה
  classify(text:string){
  //המבנה צריך להיות בדיוק זה שהאיי פי אי רוצה לקבל - לקחת את הג'סון שהוא מחזיר ולשים בביוטיפייר
    let json = {'articles':[
      {'text':text}
    ]}
    //נהפוך לסטרינג בשביל להעביר אותו ברשת
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res); //מסתכלים על הראס ורואים איך שולפים ממנה את הנתונים שאנו צריכים
        let final:string = res.body; // we want only the body
        console.log(final);
        final = final.replace('[',''); // אנחנו רוצים להוריד את הסוגריים מהמספר שמוחזר מהבודי
        final = final.replace(']',''); // אנחנו רוצים להוריד את הסוגריים מהמספר שמוחזר מהבודי
        return final; 
      })
    )

  }

  constructor(private http:HttpClient) { }
}
