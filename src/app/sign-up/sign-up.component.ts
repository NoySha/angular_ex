import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password: string;
  errorMessage:string;
  isError:boolean = false;

  constructor(private authService:AuthService, private router:Router) { } // authService-משתנה , AuthService- שם המחלקה
  // אנגולר יוצר אובייקט מסוג אוטסרוויס ומנגיש אותו למחלקה, כך אפשר להעביר מידע בין כל הקומפוננטות (הזרקה)

  OnSubmit(){
    // then is for promise and not obserbale - we return the promise. we need to register to this promise with "then"
    this.authService.signUp(this.email, this.password).then(
        res => {
          console.log('successful login;')
          this.router.navigate(['/books']); //לאחר התחברות המשתמש יעבור לבוקס
        }
    ).catch(
      err => { // err הוא אובייקט שמכיל את הודעת השגיאה
        console.log(err);
        this.isError = true;
        this.errorMessage = err.message; // רואים בקונסול שלפונקציה ארר יש שדה שנקרא message
      }
    )
  }


  ngOnInit(): void {
  }

}
