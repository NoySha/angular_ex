import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ItemsPredictionService {

  private url = "https://jz45oi2bq8.execute-api.us-east-1.amazonaws.com/example_test"


  //נשלח נתונים לאיי פי אי ונקבל חזרה תוצאה
 classify(price:number, inStock:boolean):Observable<any>{
  //המבנה צריך להיות בדיוק זה שהאיי פי אי רוצה לקבל - לקחת את הג'סון שהוא מחזיר ולשים בביוטיפייר
    let json = {
      'data': {
        'price': price,
        'inStock': inStock
      }
    }
    let body = JSON.stringify(json);
 return this.HttpClient.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res); //מסתכלים על הראס ורואים איך שולפים ממנה את הנתונים שאנו צריכים
        let final:string = res.body; // we want only the body
        final = final.replace('"',''); 
        final = final.replace('"',''); 
        return final; 
      })
    )

  }

  constructor(private HttpClient:HttpClient) { }
}
