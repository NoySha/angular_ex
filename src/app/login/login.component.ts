import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //המשתנים יגיעו לכאן מהטופס
  email:string;
  password:string; 
  errorMessage:string;
  isError:boolean = false;

  onSubmit(){
    this.auth.login(this.email,this.password).then(
      res => { //then במקום subscribe, פרומיס הוא כמו אובסרבל רק שיודע לתת נתון אחד במקום הרבה
      // המידע שחזר נמצא בתוך ה -res (אם ההתחברות הייתה מוצלחת או לא)
      console.log(res);
      this.router.navigate(['/home']); //לאחר התחברות המשתמש יעבור לבוקס

      
    }
    ).catch(
      err => { // err הוא אובייקט שמכיל את הודעת השגיאה
        console.log(err);
        this.isError = true;
        this.errorMessage = err.message; // רואים בקונסול שלפונקציה ארר יש שדה שנקרא message
      }
    )
  }
  
  constructor(private auth:AuthService, private router:Router) { } //מזריקים בשביל שיהיה ניתן להשתמש בסרוויס

  ngOnInit(): void {
  }

}
