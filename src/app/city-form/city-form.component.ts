import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css']
})
export class CityFormComponent implements OnInit {

    //Object is jason - אנו כותבים מערך של אובייקטים
  cities:Object[] = [{id:1,name:'Jerusalem'},{id:2,name:'London'},{id:3,name:'Paris'},{id:4,name:'blabla'}];
  city:string; 

  onSubmit(){
        //הפונקציה מנווטת לקומפוננט של טמפרטורה ומעבירה את העיר הנבחרת
    this.router.navigate(['/temperatures',this.city])
  }

  constructor(private router:Router) { } //אנו צריכים את ראוטר מכיוון שנרצה לשלוח את העיר שהמשתמש יבחר לטמפלייט של הטמפרטורות

  ngOnInit(): void {
  }

}
