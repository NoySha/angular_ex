import { Customer } from './interfaces/customer';
import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customerCollection:AngularFirestoreCollection;


public getCustomers(userId){ // אנו רוצים ספרים של יוזר מסוים, ולכן נכניס את היוזר כמשתנה 
    this.customerCollection = this.db.collection(`users/${userId}/customers`, ref => ref.orderBy('name', 'asc')) //רפרנס לקולקשיין של בוקס (המיקום שלו)
    return this.customerCollection.snapshotChanges().pipe(map( 
       collection => collection.map(
        document => {
          const data = document.payload.doc.data(); //  כל השדות של כל ספר נמצאים בדטא- payload.doc.data - פוקנציה שמורה של פייר סטור
          data.id = document.payload.doc.id; // מוסיפים לדטא את השדה איי די של הספר
          console.log(data)
          return data; 
        }
        )
      )) 
     }

public deleteCustomers(userId, cid){
  this.db.doc(`users/${userId}/customers/${cid}`).delete()
}

public updateCustomer(userId,id, name, income, education){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {name:name, income:income, education:education}
  )
}

public addCustomer(userId, name, income, education){
  this.db.collection(`users/${userId}/customers`).add(
    {name:name, income:income, education:education}
  )
}

public addCustomerWithPred(userId, name, income, education, result){
  console.log('in addCustomerWithPred')
  this.db.collection(`users/${userId}/customers`).add(
    {name:name, income:income, education:education, result:result}
  )
}

public savePredict(userId, id,result){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {result:result}
  )
}


  constructor(private db:AngularFirestore) { }
}
