import { TestBed } from '@angular/core/testing';

import { ItemsPredictionService } from './items-prediction.service';

describe('ItemsPredictionService', () => {
  let service: ItemsPredictionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemsPredictionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
