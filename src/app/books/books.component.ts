import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Book } from '../interfaces/book';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  //books;
  books$;//מוסכמה ל observele
  books:Book[];
  userId:string;
  editstate = [];
  addBookFormOpen = false;
  lastDocumentsArrive;

  panelOpenState = false;

  constructor(private booksSerivice:BooksService, public authService:AuthService) { }

  backPage(){
    this.books$ = this.booksSerivice.getBooksBackPage(this.userId);
    this.books$.subscribe(
      docs => {
        this.books = [];
        for(let document of docs){
          const book:Book = document.payload.doc.data();
          book.id = document.payload.doc.id;
          this.books.push(book);
        }
      }
   )

  }

  nextPage(){
        this.books$ = this.booksSerivice.getBooks(this.userId,this.lastDocumentsArrive);
        this.books$.subscribe(
          docs => {
            this.lastDocumentsArrive = docs[docs.length-1].payload.doc; //האלמנט האחרון שהגיע
            this.books = [];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
       )
  }

  ngOnInit(): void {
    //this.booksSerivice.addBooks(); // הפונקציה לא שולחת נתונים, אלא רק מוסיפה עוד ספרים למערך. הנתונים נשלחים דרך האובסרבל, מוסיף כל כמה שניות את הספר
    //this.books$ = this.booksSerivice.getBooks();
    //ביצוע רישום ל- observable 
    // על מנת לקבל את המידע
    //this.books$.subscribe(books => this.books = books);

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid; //uid come from  user.ts file in interface
        this.books$ = this.booksSerivice.getBooks(this.userId,null);
        //תוספת משיעור 4.1 בשביל דפדפוף
        this.books$.subscribe(
          docs => {
            this.lastDocumentsArrive = docs[docs.length-1].payload.doc;
            this.books = [];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
        
        )
      }
    )

  }

  deleteBook(id:string){ // id of the book comes from the template
    this.booksSerivice.deleteBook(this.userId,id);
  }

  update(book:Book){ 
    this.booksSerivice.updateBook(this.userId, book.id, book.title, book.author);
  }

  add(book:Book){
    this.booksSerivice.addBook(this.userId, book.title, book.author);
  }

}
