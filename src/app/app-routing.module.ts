import { ApiComponent } from './api/api.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { CityFormComponent } from './city-form/city-form.component';
import { ClassifyComponent } from './classify/classify.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TableCustomersComponent } from './table-customers/table-customers.component';
import { TryComponent } from './try/try.component';
import { HomeComponent } from './home/home.component';
import { CustomerFormAddComponent } from './customer-form-add/customer-form-add.component';
import { ShowPerdictionComponent } from './show-perdiction/show-perdiction.component';
import { ItemsComponent } from './items/items.component';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'classify/:network', component: ClassifyComponent },
  { path: 'city', component: CityFormComponent },
  { path: 'api', component: ApiComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent},
  { path: 'customers', component: TableCustomersComponent},
  { path: 'try/:letter', component: TryComponent},
  { path: 'home', component: HomeComponent},
  { path: 'customerformadd', component: CustomerFormAddComponent},
  { path: 'showpred/:name/:education', component: ShowPerdictionComponent},
  { path: 'items', component: ItemsComponent},

  //if url not exist, the user will redirect to NotFoundComponent
  {path: '404', component:NotFoundComponent},
  {path: '**', redirectTo: '/404'}



  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
