import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  network;
  selectedNetwork;
  networks: string[] = ['cnn','bbc','nbc'];
  categoryImage;

  text:string;
  category:string = 'no category';

  classify(){
    this.ClassifyService.classify(this.text).subscribe(
      res => {
        console.log(res); // קיבלנו כאן את המספר 3
        this.category = this.ClassifyService.categories[res]; //מתוך המערך נרצה את האובייקט הזה
        this.categoryImage = this.ImageService.images[res]; // משיכת התמונה המתאימה למספר שקיבלנו
      }
    )

  }


  //צריך את מה שיש בתוך הקונסטרקטור בשביל לתפוס את ה bbc
  constructor(private route:ActivatedRoute, private ClassifyService:ClassifyService, private ImageService:ImageService) { }

  ngOnInit(): void {
    //לתפוס The bbc from the url
    this.selectedNetwork = this.route.snapshot.params.network; //input into selectedNetwork what comes from the url of the routh called network

  }

}

