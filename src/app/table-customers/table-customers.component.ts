import { CustomerPredictionService } from './../customer-prediction.service';
import { Customer } from './../interfaces/customer';
import { ClassifyService } from './../classify.service';
import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-customers',
  templateUrl: './table-customers.component.html',
  styleUrls: ['./table-customers.component.css']
})
export class TableCustomersComponent implements OnInit {


  userId;
  //customers$
  id;
  customers = [];
  closeEdit:boolean = true;
  closeAdd:boolean = true;
  prdictClick = [];
  education;
  income;
  prob;
  returnOrNot;

  

  delete(cid){
    this.CustomersService.deleteCustomers(this.userId, cid)
  }

  update(customer:Customer){
    this.CustomersService.updateCustomer(this.userId,customer.id, customer.name, customer.income, customer.education )
  }

  add(customer:Customer){
    this.CustomersService.addCustomer(this.userId,customer.name, customer.income, customer.education)
  }

  constructor(public AuthService:AuthService, private CustomersService:CustomersService,private CustomerPredictionService:CustomerPredictionService) { }


  predict(index){
    this.customers[index].result = 'Will difault';
    this.CustomerPredictionService.classify(this.customers[index].education,this.customers[index].income).subscribe(
      res=> {
        console.log(res);
        if (res > 0.5){
          var result = 'yes';
        }
        else {
           var result = 'no';
        }
        this.customers[index].result = result;
      }
    )
  }

  SavePredict(index){
    this.customers[index].saved = true; 
    console.log(this.customers[index].saved)
    console.log(this.customers[index].result)
    this.CustomersService.savePredict(this.userId,this.customers[index].id,this.customers[index].result)
  }

  ngOnInit(): void {
      this.AuthService.getUser().subscribe(
      user => {
      this.userId = user.uid
      //this.customers$ =  this.CustomersService.getCustomers(this.userId)
      // in html change to  "let customer of customers$ | async"
      this.CustomersService.getCustomers(this.userId).subscribe(
      customers =>{
        this.customers = customers})
      })
  }


  displayedColumns: string[] = ['name', 'education', 'income', 'prob', 'predict', 'result'];

}
