import { Observable } from 'rxjs';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})

export class TemperaturesComponent implements OnInit {
  city:string; 
  temperature:number;
  image:string;
  country;
  lat;
  lon;
  weatherData$:Observable<Weather>; //דולר זה מוסכמה של אובסרבל
  // לכל משתנה נגדיר דאטה טייפ -המשתנה האחרון פולט נתונים מסוג וודר שנמצא בקובץ האינטרפייס
  hasError:Boolean = false;
  errorMessage:string;

  constructor(private route:ActivatedRoute, private WeatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city; 
    // נרשמים לאובסרבל ואנו תופסים את הנתונים משם. מתוך הנתונים שיגיעו לדאטא, נשלוף את התמונה ואת הטמפרטורה
    this.weatherData$ = this.WeatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe( //אם הכל טוב סבסקרייב מפעילה את הפונקציה הראשונה ואם יש שגיאה את השנייה
      data => { // הנתונים מההרשמה מגיעים למשתנה דטא ואז עושים השמה למשתנים במחלקה לפי הנתונים שהגיעו מהאיי פי איי
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lat = data.lat;
        this.lon = data.lon;

      },
      error =>{
        console.log(error.message); // מי שיצר את ארוור זה הסרוויס בפונקציה catchError
        this.hasError = true;
        this.errorMessage = error.message;
      }
      )
  }

}
