import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Api } from './interfaces/api';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  [x: string]: any;
  private URL = "https://jsonplaceholder.typicode.com/posts/"; 


  constructor(private http:HttpClient) { }

  //לא צריך פונקציה שממירה את הנתונים כמו במקרה של הטמפרטורה כי אנחנו רוצים להציג את הנתונים כמו שנמשוך אותם
  searchApiData():Observable<Api>{ // מושכים לאובסרבל נתונים מסוג אייפיאיי (אינטרפייס)
    return this.http.get<Api>(this.URL);
    
  }

}