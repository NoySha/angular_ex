import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Api } from '../interfaces/api';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {
  posts$:Observable<Api>; //דולר זה מוסכמה של אובסרבל

 
  constructor(private route:ActivatedRoute, private ApiService:ApiService) { }


  ngOnInit(): void {
    this.posts$ = this.ApiService.searchApiData();
    // אנו לא מבצעים כאן רישום לאובסרבל , נרשם אליו בקובץ הטמפלייט (אפשר לעשות אחת מאופציות)
   
     }


}
