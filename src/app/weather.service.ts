import { TemperaturesComponent } from './temperatures/temperatures.component';
import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, ɵConsole } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { Weather } from './interfaces/weather';
import { catchError, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q="; 
  private KEY = "7f9b36e4754ec098afd4c2f6a8234d74"; 
  private IMP = "units=metric";  // באיזה יחידות אנו רוצים את הנתונים

  constructor(private http:HttpClient) { }

  //פונקציה שמבצעת את התקשורת עם השרת
  // קוראת את ה -API
  // הפונקציה מקבלת שם של עיר ומחזירה אובסרבל מסוג וודר
  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(// we thake the object http from type httpClient and we put it to function get and from the get function we will get WaetherRaw from the url
    map(data => this.transformWeatherData(data)),//ממיר את הנתונים לסוג אחר - הדטא זה הוודר הגולמי
    catchError(this.handleError)
    )
  }

  // פונקציית שירות שאלייה יפנה כאשר תהיה שגיאה
  //HttpClient מפנה
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server erorr') //|| 'Server erorr - אם מה שלפני יהיה ריק זאת ההודעה שתוצג במקום שהמערכת תקרוס
  }
  // לוקחת את הנתונים הגולמיים והופכת לנתונים שאנו רוצים, ממירה את הקוד של האייקון לקישור
  private transformWeatherData(data:WeatherRaw):Weather{
      return {
        name:data.name, //name of weather will be name of weather-raw
        country:data.sys.country,
        image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`, //thaken from openWheather - when they save the image
        description:data.weather[0].description,
        temperature: data.main.temp,
        lat:data.coord.lat,
        lon:data.coord.lon
      }
    
  }



}
